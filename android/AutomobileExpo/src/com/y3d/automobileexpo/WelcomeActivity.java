package com.y3d.automobileexpo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;

public class WelcomeActivity extends Activity{

	private LayoutParams params;
	private ImageView mainImageViewUp, mainImageViewDown;

	private SharedPreferences shaPreferences;
	private Editor editor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_welcome);

		shaPreferences = this.getSharedPreferences("user", this.MODE_PRIVATE);
		editor = shaPreferences.edit();

		WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
		int windowsWidth = wm.getDefaultDisplay().getWidth();
		int windowsHeight = wm.getDefaultDisplay().getHeight();

		int widthUp = (int) (windowsWidth * 0.6);
		int heightUp = widthUp;

		mainImageViewUp = (ImageView) findViewById(R.id.welcomeImageView);
		params = (LayoutParams) mainImageViewUp.getLayoutParams();
		params.width = widthUp;
		params.height = heightUp;
		mainImageViewUp.setLayoutParams(params);
		mainImageViewUp.setBackgroundResource(R.drawable.ic_launcher);

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				Intent mainIntent = new Intent(WelcomeActivity.this, MainActivity.class);
				startActivity(mainIntent);
				finish();
			}

		}, 3000);

	}



}
