package com.y3d.automobileexpo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.y3d.automobileexpo.ui.PullDownView;
import com.y3d.automobileexpo.ui.PullDownView.OnPullDownListener;

public class BrandActivity extends BaseActivity implements OnPullDownListener, OnItemClickListener {

	private ListView mListView;
	private PullDownView mPullDownView;
	private List<Map<String, Object>> list;
	private ListBrandAdapter adapter;

	protected ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;

	private Gson gson;
	private int page = 1;
	
	private ImageButton returnMainBtn, searchBtn;
	private TextView titleNameTextView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
		
		returnMainBtn = (ImageButton) this.findViewById(R.id.title_back_imageBtn);
		returnMainBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(BrandActivity.this, MainActivity.class);
				startActivity(intent);
				finish();
			}
		});

		titleNameTextView = (TextView) findViewById(R.id.title_name_textView);
		titleNameTextView.setText("参展品牌");

		searchBtn = (ImageButton) findViewById(R.id.title_navigation_imageBtn);
		searchBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(BrandActivity.this, SearchActivity.class);
				startActivity(intent);
			}
		});
		
		list = new ArrayList<Map<String, Object>>();
		mPullDownView = (PullDownView) findViewById(R.id.list);
		mPullDownView.setOnPullDownListener(this);
		mListView = mPullDownView.getListView();
		mListView.setOnItemClickListener(this);
		adapter = new ListBrandAdapter(this);
		mListView.setAdapter(adapter);
		
		gson = new Gson();
		getData(page);
	}
	
	private void getData(int page) {
		for (int i = 0; i < 10; i++) {
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("image", R.drawable.ic_launcher);
			map.put("content", i);
			list.add(map);
		}
		
		adapter.notifyDataSetChanged();
		mPullDownView.notifyDidLoad();
		mPullDownView.enableAutoFetchMore(false, 1);
		
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

	}

	@Override
	public void onRefresh() {
		mPullDownView.notifyDidRefresh();
	}

	@Override
	public void onMore() {
		mPullDownView.notifyDidMore();
	}
	
	class ViewHolder {
		ImageView imageView;
		TextView contentText;
	}

	class ListBrandAdapter extends BaseAdapter {
		private LayoutInflater mInflater;

		public ListBrandAdapter(Context context) {
			this.mInflater = LayoutInflater.from(context);
		}

		@Override
		public int getCount() {
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			final ViewHolder holder;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.list_brand_adapter, null);
				holder = new ViewHolder();

				holder.imageView = (ImageView) convertView.findViewById(R.id.list_brand_imageView);
				holder.contentText = (TextView) convertView.findViewById(R.id.list_brand_textView);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			holder.imageView.setBackgroundResource(Integer.valueOf(list.get(position).get("image").toString()));
			
			if (list.get(position).get("content") != null) {
				holder.contentText.setText(list.get(position).get("content").toString());
			}
			return convertView;
		

		}
	}

}
