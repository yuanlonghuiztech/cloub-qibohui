package com.y3d.automobileexpo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

public class ExhibitionHallActivity extends BaseActivity {

	private ImageView exHallImageView;
	private TextView exHallTextView;

	private LayoutParams params;

	private ImageButton returnMainBtn, searchBtn;
	private TextView titleNameTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_exhibitionhall);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);

		returnMainBtn = (ImageButton) this.findViewById(R.id.title_back_imageBtn);
		returnMainBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ExhibitionHallActivity.this, MainActivity.class);
				startActivity(intent);
				finish();
			}
		});

		titleNameTextView = (TextView) findViewById(R.id.title_name_textView);
		titleNameTextView.setText("展馆介绍");

		searchBtn = (ImageButton) findViewById(R.id.title_navigation_imageBtn);
		searchBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ExhibitionHallActivity.this, SearchActivity.class);
				startActivity(intent);
			}
		});

		exHallImageView = (ImageView) findViewById(R.id.exhibitionhall_imageView);
		exHallTextView = (TextView) findViewById(R.id.exhibitionhall_textView);

		WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
		int windowsHeight = wm.getDefaultDisplay().getHeight();

		int height = (int) (windowsHeight * 0.4);
		params = (LayoutParams) exHallImageView.getLayoutParams();
		params.height = height;
		exHallImageView.setLayoutParams(params);

	}
}
