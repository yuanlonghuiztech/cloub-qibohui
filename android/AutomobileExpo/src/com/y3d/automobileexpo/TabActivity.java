package com.y3d.automobileexpo;

import android.app.ActivityGroup;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.TabHost.OnTabChangeListener;

public class TabActivity extends ActivityGroup implements OnTabChangeListener {

	private TabHost tabHost = null;
	private LayoutInflater mInflater = null;

	private ImageButton returnMainBtn, searchBtn;
	private TextView titleNameTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.tabs);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);

		returnMainBtn = (ImageButton) this.findViewById(R.id.title_back_imageBtn);
		returnMainBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(TabActivity.this, MainActivity.class);
				startActivity(intent);
				finish();
			}
		});

		titleNameTextView = (TextView) findViewById(R.id.title_name_textView);

		searchBtn = (ImageButton) findViewById(R.id.title_navigation_imageBtn);
		searchBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(TabActivity.this, SearchActivity.class);
				startActivity(intent);
			}
		});

		mInflater = LayoutInflater.from(this);

		tabHost = (TabHost) findViewById(R.id.mytabhost);
		tabHost.setup(this.getLocalActivityManager());
		Intent intent;

		intent = new Intent(this, CollectActivity.class);
		View tab1Spec = mInflater.inflate(R.layout.tab1_spec, null);
		tabHost.addTab(tabHost.newTabSpec("tab1").setIndicator(tab1Spec).setContent(intent));

		intent = new Intent(this, FavorableActivity.class);
		View tab2Spec = mInflater.inflate(R.layout.tab2_spec, null);
		tabHost.addTab(tabHost.newTabSpec("tab2").setIndicator(tab2Spec).setContent(intent));

		intent = new Intent(this, MessageActivity.class);
		View tab3Spec = mInflater.inflate(R.layout.tab3_spec, null);
		tabHost.addTab(tabHost.newTabSpec("tab3").setIndicator(tab3Spec).setContent(intent));

		intent = new Intent(this, BeerandSkittlesActivity.class);
		View tab4Spec = mInflater.inflate(R.layout.tab4_spec, null);
		tabHost.addTab(tabHost.newTabSpec("tab4").setIndicator(tab4Spec).setContent(intent));

		intent = new Intent(this, SettingActivity.class);
		View tab5Spec = mInflater.inflate(R.layout.tab5_spec, null);
		tabHost.addTab(tabHost.newTabSpec("tab5").setIndicator(tab5Spec).setContent(intent));

		String tabId = getIntent().getStringExtra("tab");
		if (tabId.equals("1")) {
			tabHost.setCurrentTab(1);
			titleNameTextView.setText("限时惠购");
		} else if (tabId.equals("3")) {
			tabHost.setCurrentTab(3);
			titleNameTextView.setText("吃喝玩乐");
		} else {
			tabHost.setCurrentTab(0);
			titleNameTextView.setText("收藏");
		}

		tabHost.setOnTabChangedListener(this);
	}

	@Override
	public void onTabChanged(String tabId) {
		if (tabHost.getCurrentTab() == 0) {
			titleNameTextView.setText("收藏");
		} else if (tabHost.getCurrentTab() == 1) {
			titleNameTextView.setText("限时惠购");
		} else if (tabHost.getCurrentTab() == 2) {
			titleNameTextView.setText("展会资讯");
		} else if (tabHost.getCurrentTab() == 3) {
			titleNameTextView.setText("吃喝玩乐");
		} else if (tabHost.getCurrentTab() == 4) {
			titleNameTextView.setText("设置");
		}
	}
}
