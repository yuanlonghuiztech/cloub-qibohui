package com.y3d.automobileexpo;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.RelativeLayout;

public class MainActivity extends Activity implements OnClickListener {

	private LayoutParams params;

	private ImageView mainImageView;
	private ImageButton oneImageBtn, twoImageBtn, threeImageBtn, fourImageBtn, fiveImageBtn, sixImageBtn, sevenImageBtn;
	private LinearLayout mainTopLayout, mainLayout;
	private RelativeLayout mainBottomLayout;
	private Button mainBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);

		// 组件实例化
		mainTopLayout = (LinearLayout) findViewById(R.id.mainTopLayout);
		mainLayout = (LinearLayout) findViewById(R.id.main_layout);
		mainBottomLayout = (RelativeLayout) findViewById(R.id.main_bottom_layout);
		mainImageView = (ImageView) findViewById(R.id.main_imageView);
		oneImageBtn = (ImageButton) findViewById(R.id.main_imageBtn_one);
		twoImageBtn = (ImageButton) findViewById(R.id.main_imageBtn_two);
		threeImageBtn = (ImageButton) findViewById(R.id.main_imageBtn_three);
		fourImageBtn = (ImageButton) findViewById(R.id.main_imageBtn_four);
		fiveImageBtn = (ImageButton) findViewById(R.id.main_imageBtn_five);
		sixImageBtn = (ImageButton) findViewById(R.id.main_imageBtn_six);
		sevenImageBtn = (ImageButton) findViewById(R.id.main_imageBtn_seven);
		mainBtn = (Button) findViewById(R.id.main_button);

		// 首页布局按比例调整
		WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
		int windowsHeight = wm.getDefaultDisplay().getHeight();

		int height = (int) (windowsHeight * 0.4);
		params = (LayoutParams) mainImageView.getLayoutParams();
		params.height = height;
		mainImageView.setLayoutParams(params);

		int heightLayout = (int) (windowsHeight * 0.35);
		params = (LayoutParams) mainLayout.getLayoutParams();
		params.height = heightLayout;
		mainLayout.setLayoutParams(params);

		// 按钮监听点击事件
		oneImageBtn.setOnClickListener(this);
		twoImageBtn.setOnClickListener(this);
		threeImageBtn.setOnClickListener(this);
		fourImageBtn.setOnClickListener(this);
		fiveImageBtn.setOnClickListener(this);
		sixImageBtn.setOnClickListener(this);
		sevenImageBtn.setOnClickListener(this);
		mainBtn.setOnClickListener(this);

	}

	// 按钮点击事件
	@Override
	public void onClick(View v) {
		Intent intent;
		switch (v.getId()) {
		case R.id.main_imageBtn_one:
			intent = new Intent(this, TabActivity.class);
			// 限时惠购 tab-1
			intent.putExtra("tab", "1");
			startActivity(intent);
			break;
		case R.id.main_imageBtn_two:
			intent = new Intent(this, NavigationActivity.class);
			// 展场导航
			startActivity(intent);
			break;
		case R.id.main_imageBtn_three:
			intent = new Intent(this, TabActivity.class);
			// 搜索
			intent.putExtra("tab", "3");
			startActivity(intent);
			break;
		case R.id.main_imageBtn_four:
			intent = new Intent(this, BrandActivity.class);
			// 参展品牌
			startActivity(intent);
			break;
		case R.id.main_imageBtn_five:
			intent = new Intent(this, MessageActivity.class);
			// 展会资讯 tab-3
			startActivity(intent);
			break;
		case R.id.main_imageBtn_six:
			intent = new Intent(this, BeerandSkittlesActivity.class);
			// 吃喝玩乐
			startActivity(intent);
			break;
		case R.id.main_imageBtn_seven:
			intent = new Intent(this, CarLoanActivity.class);
			// 直通车贷
			startActivity(intent);
			break;
		case R.id.main_button:
			intent = new Intent(this, ExhibitionHallActivity.class);
			// 展馆介绍
			startActivity(intent);
			break;

		default:
			break;
		}
	}
}
